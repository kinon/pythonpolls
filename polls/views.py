from django.shortcuts import get_object_or_404, render
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.views import generic

from polls.models import Opcion, Pregunta


class IndexView(generic.ListView):
    template_name = 'polls/index.html'
    context_object_name = 'latest_question_list'

    def get_queryset(self):
        """Return the last five published Preguntas."""
        return Pregunta.objects.order_by('-pub_date')[:5]


class DetailView(generic.DetailView):
    model = Pregunta
    template_name = 'polls/detail.html'


class ResultsView(generic.DetailView):
    model = Pregunta
    template_name = 'polls/results.html'


def vote(request, Pregunta_id):
    p = get_object_or_404(Pregunta, pk=pregunta_id)
    try:
        selected_opcion = p.opcion_set.get(pk=request.POST['opcion'])
    except (KeyError, Opcion.DoesNotExist):
        # Redisplay the Pregunta voting form.
        return render(request, 'polls/detail.html', {
            'pregunta': p,
            'error_message': "You didn't select a opcion.",
        })
    else:
        selected_opcion.votes += 1
        selected_opcion.save()
        # Always return an HttpResponseRedirect after successfully dealing
        # with POST data. This prevents data from being posted twice if a
        # user hits the Back button.
        return HttpResponseRedirect(reverse('polls:results', args=(p.id,)))
