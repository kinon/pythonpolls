from django.contrib import admin
from polls.models import Opcion, Pregunta


class OpcionInline(admin.TabularInline):
    model = Opcion
    extra = 3


class PreguntaAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,               {'fields': ['pregunta_texto']}),
        ('Date information', {'fields': ['pub_date'], 'classes': ['collapse']}),
    ]
    inlines = [OpcionInline]
    list_display = ('pregunta_texto', 'pub_date', 'publicada_recientemente')
    list_filter = ['pub_date']
    search_fields = ['question_text']


admin.site.register(Pregunta, PreguntaAdmin)
