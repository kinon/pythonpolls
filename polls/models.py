import datetime

from django.db import models
from django.utils import timezone


class Pregunta(models.Model):
    pregunta_texto = models.CharField(max_length=200)
    pub_date = models.DateTimeField('fecha publicacion')
    def __str__(self):
        return self.pregunta_texto
    def publicada_recientemente(self):
        return self.pub_date >= timezone.now() - datetime.timedelta(days=1)
    publicada_recientemente.admin_order_field = 'pub_date'
    publicada_recientemente.boolean = True


class Opcion(models.Model):
    pregunta = models.ForeignKey(Pregunta)
    opcion_texto = models.CharField(max_length=200)
    votos = models.IntegerField(default=0)
    def __str__(self):
        return self.opcion_texto
